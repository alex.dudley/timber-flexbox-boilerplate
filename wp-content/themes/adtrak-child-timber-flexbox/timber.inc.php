<?php 

$autoload = __DIR__ . '/vendor/autoload.php';

if( file_exists( $autoload ) ) :

	require_once( $autoload );
	$timber = new Timber\Timber();

endif;

if( !class_exists( 'Timber' ) ) :
	add_action( 'admin_notices', function() {
		echo "<div class=\"error\"><p>Timber does not appear to be available. Check composer install</p></div>";
	});
endif;

// Some Timber setup
Timber::$dirname = array( 'templates', 'views' );



class Adtrak extends Timber\Site {
	/** Add timber support. */
	public function __construct() {
		add_action( 'after_setup_theme', array( $this, 'theme_supports' ) );
		add_filter( 'timber/context', array( $this, 'add_to_context' ) );
		parent::__construct();
	}

	public function fetchCritical($path) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $path);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$output = curl_exec($ch);
		curl_close($ch);
		return $output;
	}

	/** This is where you add some context
	 *
	 * @param string $context context['this'] Being the Twig's {{ this }}.
	 */
	public function add_to_context( $context ) {
		$context['primary_menu'] = new Timber\Menu('Primary Menu');
		$context['secondary_menu'] = new Timber\Menu('Secondary Menu');
		$context['information_menu'] = new Timber\Menu('Information Menu');
		$context['is_front_page'] = is_front_page();
		$context['options'] = get_fields('option');
		$context['site'] = $this;
		return $context;
	}
	public function theme_supports() {
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'menus' );
	}
}

new Adtrak();